$(function(){
	//Avoiding manual scroll and automatic scroll interference
	$("html, body").on("scroll mousedown DOMMouseScroll mousewheel keyup", function(){	
	    $('html, body').stop();
   	});
	$("html, body").stop().animate({scrollTop:0}, 1000, 'easeOutQuart');

	$("#FirstPageRouter").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()}, 1000, 'easeOutQuart');
	});
	$("#SecondPageRouter").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()+$(".FirstPage").height()}, 1000, 'easeOutQuart');
	});
	$("#ThirdPageRouter").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()+$(".FirstPage").height()*2}, 1000, 'easeOutQuart');
	});

	$(".RouterIcon0").click(function(){
		$("html, body").stop().animate({scrollTop:0}, 1000, 'easeOutQuart');
	});
	$(".RouterIcon1").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()}, 1000, 'easeOutQuart');
	});
	$(".RouterIcon2").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()+$(".FirstPage").height()}, 1000, 'easeOutQuart');
	});
	$(".RouterIcon3").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()+$(".FirstPage").height()*2}, 1000, 'easeOutQuart');
	});
	$(".RouterIcon4").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()+$(".FirstPage").height()*3}, 1000, 'easeOutQuart');
	});

	$(".ArrowBottom").click(function(){
		$("html, body").stop().animate({scrollTop:$(".LandingPageBk").height()}, 1000, 'easeOutQuart');
	});

	$(".RouterIcon0").mouseover(function(){
		$(".RouterContainerText").text("Home").show();
	}).mouseout(function(){
		$(".RouterContainerText").hide();
	});
	$(".RouterIcon1").mouseover(function(){
		$(".RouterContainerText").text("Ramachandran Plot").show();
	}).mouseout(function(){
		$(".RouterContainerText").hide();
	});
	$(".RouterIcon2").mouseover(function(){
		$(".RouterContainerText").text("B Factor Plot").show();
	}).mouseout(function(){
		$(".RouterContainerText").hide();
	});
	$(".RouterIcon3").mouseover(function(){
		$(".RouterContainerText").text("Correlation Plot").show();
	}).mouseout(function(){
		$(".RouterContainerText").hide();
	});
	$(".RouterIcon4").mouseover(function(){
		$(".RouterContainerText").text("Comparison of Charts").show();
	}).mouseout(function(){
		$(".RouterContainerText").hide();
	});



	var InitalError = setInterval(function(){
		$("#chart1").text("Search for Ramachandran Plot.").removeClass("shiny-output-error")
		$("#chart2").text("Search for Ramachandran Plot.").removeClass("shiny-output-error")
		$("#chart3").text("Search for Ramachandran Plot.").removeClass("shiny-output-error")
		$("#chart4").text("Search for Ramachandran Plot.").removeClass("shiny-output-error")
		$("#chart5").text("Search for Ramachandran Plot.").removeClass("shiny-output-error")
		$("#chart6").text("Search for B Factor Plot.").removeClass("shiny-output-error")
		$("#chart7").text("Search for B Factor Plot.").removeClass("shiny-output-error")
		$("#chart8").text("Search for B Factor Plot.").removeClass("shiny-output-error")
		$("#chart9").text("Search for B Factor Plot.").removeClass("shiny-output-error")
		$("#chart10").text("Search for B Factor Plot.").removeClass("shiny-output-error")
		$("#chart11").text("Search for Correlation Plot.").removeClass("shiny-output-error")
		$("#chart12").text("Search for Correlation Plot.").removeClass("shiny-output-error")
		$("#chart13").text("Search for Correlation Plot.").removeClass("shiny-output-error")
		$("#chart14").text("Search for Correlation Plot.").removeClass("shiny-output-error")
		$("#chart15").text("Search for Correlation Plot.").removeClass("shiny-output-error")

		if(!$("#chart1").hasClass("shiny-output-error") && !$("#chart2").hasClass("shiny-output-error") && !$("#chart3").hasClass("shiny-output-error") && !$("#chart4").hasClass("shiny-output-error") && !$("#chart5").hasClass("shiny-output-error") && !$("#chart6").hasClass("shiny-output-error") && !$("#chart7").hasClass("shiny-output-error") && !$("#chart8").hasClass("shiny-output-error") && !$("#chart9").hasClass("shiny-output-error") && !$("#chart10").hasClass("shiny-output-error") && !$("#chart11").hasClass("shiny-output-error") && !$("#chart12").hasClass("shiny-output-error") && !$("#chart13").hasClass("shiny-output-error") && !$("#chart14").hasClass("shiny-output-error") && !$("#chart15").hasClass("shiny-output-error")){
			clearInterval(InitalError);
		}
	}, 1000);

	/*pdb 1 */
	$("#pdb1").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb1").val()==""){
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
				$("#ok1").click();
			}
	    }
	});

	$("#ok1").click(function(){
		if($("#pdb1").val()==""){
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
		}
		
		$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text($("#pdb1").val())
		if($("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
	});

	/*pdb 2 */
	$("#pdb2").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb2").val()==""){
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
				$("#ok2").click();
			}
	    }
	});
	$("#ok2").click(function(){
		if($("#pdb2").val()==""){
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
		}
		
		$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text($("#pdb2").val())
		if($("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerRC").children("#chart1").remove();
	});

	/*pdb 3 */
	$("#pdb3").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb3").val()==""){
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
				$("#ok3").click();
			}
	    }
	});
	$("#ok3").click(function(){
		if($("#pdb3").val()==""){
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
		}
		
		$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text($("#pdb3").val())
		if($("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerRC").children("#chart2").remove();
	});

	/*pdb 4 */
	$("#pdb4").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb4").val()==""){
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
				$("#ok4").click();
			}
	    }
	});
	$("#ok4").click(function(){
		if($("#pdb4").val()==""){
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
		}
		
		$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text($("#pdb4").val())
		if($("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerRC").children("#chart3").remove();
	});
	/*pdb 5 */
	$("#pdb5").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb5").val()==""){
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
				$("#ok5").click();
			}
	    }
	});
	$("#ok5").click(function(){
		if($("#pdb5").val()==""){
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot")
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").text("Ramachandran plot for")
		}
		
		$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text($("#pdb5").val())
		if($("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerRC").children("#chart4").remove();
	});

	/*pdb 6 */
	$("#pdb6").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb6").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
				$("#ok6").click();
			}
	    }
	});
	$("#ok6").click(function(){
		if($("#pdb6").val()==""){
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
		}
		
		$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text($("#pdb6").val())
		if($("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
	});

	/*pdb 7 */
	$("#pdb7").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb7").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
				$("#ok7").click();
			}
	    }
	});
	$("#ok7").click(function(){
		if($("#pdb7").val()==""){
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
		}
		
		$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text($("#pdb7").val())
		if($("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerBF").children("#chart6").remove();
	});

	/*pdb 8 */
	$("#pdb8").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb8").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
				$("#ok8").click();
			}
	    }
	});
	$("#ok8").click(function(){
		if($("#pdb8").val()==""){
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
		}
		
		$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text($("#pdb8").val())
		if($("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerBF").children("#chart7").remove();
	});

	/*pdb 9 */
	$("#pdb9").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb9").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
				$("#ok9").click();
			}
	    }
	});
	$("#ok9").click(function(){
		if($("#pdb9").val()==""){
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
		}
		
		$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text($("#pdb9").val())
		if($("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerBF").children("#chart8").remove();
	});

	/*pdb 10 */
	$("#pdb10").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb10").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
				$("#ok10").click();
			}
	    }
	});
	$("#ok10").click(function(){
		if($("#pdb10").val()==""){
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot")
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("B Factor plot for")
		}
		
		$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text($("#pdb10").val())
		if($("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerBF").children("#chart9").remove();
	});

	/*pdb 11 */
	$("#pdb11").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb11").val()==""){
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("Correlation plot")
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").text("Correlation plot for")
				$("#ok11").click();
			}
	    }
	});
	$("#ok11").click(function(){
		if($("#pdb11").val()==""){
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
		}
		
		$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text($("#pdb11").val())
		if($("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
	});

	/*pdb 12 */
	$("#pdb12").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb12").val()==""){
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
				$("#ok12").click();
			}
	    }
	});
	$("#ok12").click(function(){
		if($("#pdb12").val()==""){
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
		}
		
		$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text($("#pdb12").val())
		if($("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerCP").children("#chart11").remove();
	});

	/*pdb 13 */
	$("#pdb13").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb13").val()==""){
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
				$("#ok13").click();
			}
	    }
	});
	$("#ok13").click(function(){
		if($("#pdb13").val()==""){
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
		}
		
		$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text($("#pdb13").val())
		if($("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerCP").children("#chart12").remove();
	});

	/*pdb 14 */
	$("#pdb14").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb14").val()==""){
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
				$("#ok14").click();
			}
	    }
	});
	$("#ok14").click(function(){
		if($("#pdb14").val()==""){
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
		}
		
		$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text($("#pdb14").val())
		if($("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerCP").children("#chart13").remove();
	});

	/*pdb 15 */
	$("#pdb15").keypress(function(e) {
	    if(e.which == 13) {
	        if($("#pdb15").val()==""){
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
			}else{
				$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
				$("#ok15").click();
			}
	    }
	});
	$("#ok15").click(function(){
		if($("#pdb15").val()==""){
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot")
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text("")
		}else{
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").text("Correlation plot for")
		}
		
		$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text($("#pdb15").val())
		if($("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length >0){
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").text("Added for Comparison")
		}else{
			$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
		}
		$("#ChartContainerCP").children("#chart14").remove();
	});



	/* Add to compare panel */
	var RCcount = 1;
	var BFcount = 6;
	var CPcount = 11;

	$("#PlotcontainerTitleRC").find(".AddToCompPanel").click(function(){
		if($(this).attr("clickedonce")=="false" && $("#RecentPanelWrapperRC").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+"')" ).length ==0){
			$("#RecentPanelWrapperRC").append('<div class="row RecentContent"><div class="col-sm-10 RecentPanelText">'+$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").text()+'</div><div class="col-sm-2 RecentPanelDelete"><i class="fa fa-trash-o removeCompChart removeCompChartRC"></i></div></div>');
			$(this).attr("clickedonce",true);
			$(this).text("Added for Comparison");

			$(".removeCompChartRC").click(function(){
				$("#PlotcontainerTitleRC").find(".AddToCompPanel").attr("clickedonce",false);
				$("#PlotcontainerTitleRC").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
				$(this).parent().parent().animate({height:0},300,function(){
					$(this).remove();
				});
				$(".ComparisonHeaderRC").children(".PlotcontainerTitleThin:contains('"+$(this).parent().parent().children(".RecentPanelText").text()+"')").parent().parent().remove();
				if($(".ComparisonWrapper").length == 0){
					$(".emptyBox").show();
				}else{
					$(".emptyBox").hide();
				}
			});

			comparisonWrapperElem = $('<div class="col-md-5 ComparisonWrapper"><div class="row ComparisonHeader ComparisonHeaderRC">Plot</div><div class="row ComparisonContent"></div></div>');
			$(".ComparisonPage").append(comparisonWrapperElem);

			$("#forChart"+RCcount).hide();
			RCchartValue = $("#chart"+RCcount);
			comparisonWrapperElem.children( ".ComparisonContent" ).append(RCchartValue);
			RCchartValue.clone().prependTo("#ChartContainerRC");

			comparisonWrapperElem.children(".ComparisonHeader").empty();
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleBold").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));
			$("#PlotcontainerTitleRC").find(".PlotcontainerTitleThin").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));

			RCcount = RCcount + 1;

			$("#forChart"+RCcount).show();
			$("#chart"+RCcount).show();
		}
		if($(".ComparisonWrapper").length == 0){
			$(".emptyBox").show();
		}else{
			$(".emptyBox").hide();
		}
	});


	$("#PlotcontainerTitleBF").find(".AddToCompPanel").click(function(){
		if($(this).attr("clickedonce")=="false" && $("#RecentPanelWrapperBF").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+"')" ).length ==0){
			$("#RecentPanelWrapperBF").append('<div class="row RecentContent"><div class="col-sm-10 RecentPanelText">'+$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").text()+'</div><div class="col-sm-2 RecentPanelDelete"><i class="fa fa-trash-o removeCompChart removeCompChartBF"></i></div></div>');
			$(this).attr("clickedonce",true);
			$(this).text("Added for Comparison");

			$(".removeCompChartBF").click(function(){
				$("#PlotcontainerTitleBF").find(".AddToCompPanel").attr("clickedonce",false);
				$("#PlotcontainerTitleBF").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
				$(this).parent().parent().animate({height:0},300,function(){
					$(this).remove();
				});
				$(".ComparisonHeaderBF").children(".PlotcontainerTitleThin:contains('"+$(this).parent().parent().children(".RecentPanelText").text()+"')").parent().parent().remove();
				
				if($(".ComparisonWrapper").length == 0){
					$(".emptyBox").show();
				}else{
					$(".emptyBox").hide();
				}
			});

			comparisonWrapperElem = $('<div class="col-md-5 ComparisonWrapper"><div class="row ComparisonHeader ComparisonHeaderBF">Plot</div><div class="row ComparisonContent"></div></div>');
			$(".ComparisonPage").append(comparisonWrapperElem);

			$("#forChart"+BFcount).hide();
			BFchartValue = $("#chart"+BFcount);
			comparisonWrapperElem.children( ".ComparisonContent" ).append(BFchartValue);
			BFchartValue.clone().prependTo("#ChartContainerBF");

			comparisonWrapperElem.children(".ComparisonHeader").empty();
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleBold").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));
			$("#PlotcontainerTitleBF").find(".PlotcontainerTitleThin").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));

			BFcount = BFcount + 1;

			$("#forChart"+BFcount).show();
			$("#chart"+BFcount).show();
		}
		if($(".ComparisonWrapper").length == 0){
			$(".emptyBox").show();
		}else{
			$(".emptyBox").hide();
		}
	});

	$("#PlotcontainerTitleCP").find(".AddToCompPanel").click(function(){
		if($(this).attr("clickedonce")=="false" && $("#RecentPanelWrapperCP").find( ".RecentPanelText:contains('"+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+"')" ).length ==0){
			$("#RecentPanelWrapperCP").append('<div class="row RecentContent"><div class="col-sm-10 RecentPanelText">'+$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").text()+'</div><div class="col-sm-2 RecentPanelDelete"><i class="fa fa-trash-o removeCompChart removeCompChartCP"></i></div></div>');
			$(this).attr("clickedonce",true);
			$(this).text("Added for Comparison");

			$(".removeCompChartCP").click(function(){
				$("#PlotcontainerTitleCP").find(".AddToCompPanel").attr("clickedonce",false);
				$("#PlotcontainerTitleCP").find(".AddToCompPanel").html('Add to Compare <i class="fa fa-arrow-right"></i>')
				$(this).parent().parent().animate({height:0},300,function(){
					$(this).remove();
				});
				$(".ComparisonHeaderCP").children(".PlotcontainerTitleThin:contains('"+$(this).parent().parent().children(".RecentPanelText").text()+"')").parent().parent().remove();
				
				if($(".ComparisonWrapper").length == 0){
					$(".emptyBox").show();
				}else{
					$(".emptyBox").hide();
				}
			});

			comparisonWrapperElem = $('<div class="col-md-5 ComparisonWrapper"><div class="row ComparisonHeader ComparisonHeaderCP">Plot</div><div class="row ComparisonContent"></div></div>');
			$(".ComparisonPage").append(comparisonWrapperElem);

			$("#forChart"+CPcount).hide();
			CPchartValue = $("#chart"+CPcount);
			comparisonWrapperElem.children( ".ComparisonContent" ).append(CPchartValue);
			CPchartValue.clone().prependTo("#ChartContainerCP");

			comparisonWrapperElem.children(".ComparisonHeader").empty();
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleBold").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));
			$("#PlotcontainerTitleCP").find(".PlotcontainerTitleThin").clone().appendTo(comparisonWrapperElem.children(".ComparisonHeader"));

			CPcount = CPcount + 1;

			$("#forChart"+CPcount).show();
			$("#chart"+CPcount).show();
		}
		if($(".ComparisonWrapper").length == 0){
			$(".emptyBox").show();
		}else{
			$(".emptyBox").hide();
		}
	});
});








